require 'test_helper'

class ContactAddressTest < ActiveSupport::TestCase

  OVERLENGTH_STRING = '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'

  ### Testing Length Limits
  test 'should not allow a name over 255 characters' do
    contact_address = ContactAddress.new name: OVERLENGTH_STRING
    assert_not contact_address.save
  end

  test 'should not allow an address_1 over 255 characters' do
    contact_address = ContactAddress.new address_1: OVERLENGTH_STRING
    assert_not contact_address.save
  end

  test 'should not allow an address_2 over 255 characters' do
    contact_address = ContactAddress.new address_2: OVERLENGTH_STRING
    assert_not contact_address.save
  end

  test 'should not allow an city over 255 characters' do
    contact_address = ContactAddress.new city: OVERLENGTH_STRING
    assert_not contact_address.save
  end

  test 'should not allow an state over 255 characters' do
    contact_address = ContactAddress.new state: OVERLENGTH_STRING
    assert_not contact_address.save
  end

  test 'should not allow an postal_code over 255 characters' do
    contact_address = ContactAddress.new postal_code: OVERLENGTH_STRING
    assert_not contact_address.save
  end

  ### Testing content
  test 'should only allow letters in city names' do
    contact_address = ContactAddress.new city: '12$4'
    assert_not contact_address.save
  end

  test 'should only allow letters in state names' do
    contact_address = ContactAddress.new state: '12$4'
    assert_not contact_address.save
  end
end
