require 'test_helper'

class CcbIndividualTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "should initialize a Ccb::Individual" do
    individual = Ccb::Individual.new

    assert_instance_of Ccb::Individual, individual
  end

  test "should define addresses and phones" do
    individual = Ccb::Individual.new

    assert_respond_to individual, 'addresses'
    assert_respond_to individual, 'phones'
  end

  test 'should return an individual when find is called' do
    individual = Ccb::Individual.find(1725).first

    assert_equal 1725, individual.id.to_i
  end
end
