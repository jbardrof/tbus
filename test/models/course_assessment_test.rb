require 'test_helper'

class CourseAssessmentTest < ActiveSupport::TestCase

  test 'name must be between 3 and 100 characters' do
    course_assessment = CourseAssessment.new name: 'ab'
    assert_not course_assessment.save

    course_assessment = CourseAssessment.new name: '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
    assert_not course_assessment.save
  end

  
end
