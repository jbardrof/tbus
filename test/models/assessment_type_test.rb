require 'test_helper'

class AssessmentTypeTest < ActiveSupport::TestCase

  test 'should not save without a name' do
    assessment_type = AssessmentType.new
    assert_not assessment_type.save
  end

  test 'should not allow a name more than 255 characters' do
    long_name = '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'

    assessment_type = AssessmentType.new name: long_name
    assert_not assessment_type.save, 'Allowed a long name'
  end

  test 'should not allow a name with special characters' do
    invalid_name = '$%^&'

    assessment_type = AssessmentType.new name: invalid_name
    assert_not assessment_type.save, 'Allowed saving with invalid characters'
  end

  test 'should not allow a description with more than 255 characters' do
    long_description = '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'

    assessment_type = AssessmentType.new name: 'test', description: long_description
    assert_not assessment_type.save, 'Allowed saving with a long description'
  end
end
