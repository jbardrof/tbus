# Patches for the bootstrap form gem.
###
# The error class needs to be changed for rails 4. The class is no longer has-error, it is
# now has-danger
###
# The controls need to be changed as well for when an error occurs.
# * The label must now have a form-control-label in order for bootstrap
#   to style it as an error
# * The control itself now must also have a new class form-control-danger
#   in orer to get the proper bootstrap validation error styling.
###

module BootstrapForm
  class FormBuilder < ActionView::Helpers::FormBuilder

    ##### OVERRIDE
    def error_class
      'has-danger'
    end

    ##### OVERRIDE
    def form_group(*args, &block)
      options = args.extract_options!
      name = args.first

      options[:class] = ["form-group", options[:class]].compact.join(' ')
      options[:class] << " #{error_class}" if has_error?(name)
      options[:class] << " #{feedback_class}" if options[:icon]

      content_tag(:div, options.except(:id, :label, :help, :icon, :label_col, :control_col, :layout)) do
        # Modifies the class to satisfy bootstrap
        options[:label][:class] = "form-#{label_class}" if has_error?(name)

        label = generate_label(options[:id], name, options[:label], options[:label_col], options[:layout]) if options[:label]

        control = capture(&block).to_s
        # Modifies the class to satisfy bootstrap.
        control.gsub!(control_class, "#{control_class} #{control_class}-danger") if has_error?(name)
        control.concat(generate_help(name, options[:help]).to_s)
        control.concat(generate_icon(options[:icon])) if options[:icon]

        if get_group_layout(options[:layout]) == :horizontal
          control_class = (options[:control_col] || control_col.clone)
          unless options[:label]
            control_offset = offset_col(/([0-9]+)$/.match(options[:label_col] || @label_col))
            control_class.concat(" #{control_offset}")
          end
          control = content_tag(:div, control, class: control_class)
        end

        concat(label).concat(control)
      end
    end
  end
end
