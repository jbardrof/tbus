
module Colorizer

  def black(text);      colorize text, "0;30"; end
  def red(text);        colorize text, "0;31"; end
  def green(text);      colorize text, "0;32"; end
  def brown(text);      colorize text, "0;33"; end
  def blue(text);       colorize text, "0;34"; end
  def purple(text);     colorize text, "0;35"; end
  def cyan(text);       colorize text, "0;36"; end
  def light_gray(text); colorize text, "0;37"; end

  def dark_gray(text);    colorize text, "1;30"; end
  def light_red(text);    colorize text, "1;31"; end
  def light_green(text);  colorize text, "1;32"; end
  def light_yellow(text); colorize text, "1;33"; end
  def light_blue(text);   colorize text, "1;34"; end
  def light_purple(text); colorize text, "1;35"; end
  def light_cyan(text);   colorize text, "1;36"; end
  def white(text);        colorize text, "1;37"; end

  # colors the text based on color code
  def colorize(text, color_code)
    "\033[#{color_code}m#{text}\033[0m"
  end
end


# Black       0;30     Dark Gray     1;30
# Blue        0;34     Light Blue    1;34
# Green       0;32     Light Green   1;32
# Cyan        0;36     Light Cyan    1;36
# Red         0;31     Light Red     1;31
# Purple      0;35     Light Purple  1;35
# Brown       0;33     Yellow        1;33
# Light Gray  0;37     White         1;37
