# Custom logger
module Clogger

  # logs the message using rails debug logger
  def self.log(*message)
    ActiveSupport::Notifications.instrument('plogger', message: message) do
      Rails.logger.debug "#{colorize '------##---', 33} #{colorize message, 32} #{colorize '---##------', 33}"
    end
  end

  # colors the text based on color code
  def self.colorize(text, color_code)
    return "\033[#{color_code}m#{text}\033[0m"
  end

  def self.rest_log(options)
    request = "#{options[:request_type]} (#{options[:time]})"
    color = case options[:result].to_i
    when 200..202
      '0;32'
    when 400..501
      '0;31'
    else
      '1;37'
    end
    Rails.logger.info "  #{colorize request, color}  #{options[:url]}"
  end
end

# Timer
module Timer
  def self.clock_me(&block)
    start = Time.now
    yield
    time = (Time.now - start) * 1000
    return "#{time.round(1)}ms"
  end
end


# Black       0;30     Dark Gray     1;30
# Blue        0;34     Light Blue    1;34
# Green       0;32     Light Green   1;32
# Cyan        0;36     Light Cyan    1;36
# Red         0;31     Light Red     1;31
# Purple      0;35     Light Purple  1;35
# Brown       0;33     Yellow        1;33
# Light Gray  0;37     White         1;37
