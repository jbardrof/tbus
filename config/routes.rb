Rails.application.routes.draw do


  resources :tracks
    # Devise views and extras for invitations and user sessions.
    devise_for :users, controllers: {
      registrations: "user_registrations",
      passwords: "user_passwords"
      # Proper invitations should be sent through the active_admin interface.
      #invitations: 'users_invitations' # user_invitations_controller.rb
    }

    # Routes for administrators
    devise_for :admin_users, ActiveAdmin::Devise.config
    ActiveAdmin.routes(self)

  scope '/(:locale)' do
    resources :companions do
      collection do
        get 'search'
      end
      member do
        get 'transcript'
        get 'start_merge'
        get 'find_duplicates'
      end
    end

    resources :terms do
      member do
        get 'new_session'
        post 'create_session'
      end

      resources :sessions do
        resources :assessments, controller: :kata_assessments
        resources :learners do
          resources :assessments, controller: :kata_learner_assessments do
            collection do
              post 'final_grade'
            end
          end
        end
        member do
          post 'synchronize_users'
          post 'synchronize_group'
          post 'create_schedule'
          post 'update_schedule/:schedule_id', action: 'update_schedule'
          delete 'destroy_schedule/:schedule_id', action: 'destroy_schedule'
          post 'attendance'
          post 'tardy'
          patch :assign_to_course
          get :transcript
          get :name_tags
          get :export
        end
      end
    end

    resources :kata_schedules
    resources :companions

    resources :courses do
      resources :assessments, controller: :course_assessments
    end

    root to: 'core#index'
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
