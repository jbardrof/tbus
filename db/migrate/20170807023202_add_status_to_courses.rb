class AddStatusToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :status, :integer, default: 1, null: false
  end
end
