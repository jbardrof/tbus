class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.integer :parent_id
      t.string :external_id
      t.string :federal_tax_id

      t.timestamps
    end
  end
end
