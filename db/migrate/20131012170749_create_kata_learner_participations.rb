class CreateKataLearnerParticipations < ActiveRecord::Migration
  def change
    create_table :kata_learner_participations do |t|
      t.references :kata_learner, index: true
      t.references :kata_schedule, index: true
      t.boolean :on_time, default: true
      t.boolean :participated

      t.timestamps
    end
  end
end
