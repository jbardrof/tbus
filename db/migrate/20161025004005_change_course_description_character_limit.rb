class ChangeCourseDescriptionCharacterLimit < ActiveRecord::Migration
  def change
    change_column :courses, :description, :string, limit: 1000
  end
end
