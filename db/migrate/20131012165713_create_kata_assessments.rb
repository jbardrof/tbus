class CreateKataAssessments < ActiveRecord::Migration
  def change
    create_table :kata_assessments do |t|
      t.references :kata, index: true
      t.references :assessment_type, index: true
      t.integer :weight
      t.string :name
      t.references :kata_schedule, index: true

      t.timestamps
    end
  end
end
