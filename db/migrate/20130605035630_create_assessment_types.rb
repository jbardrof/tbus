class CreateAssessmentTypes < ActiveRecord::Migration
  def change
    create_table :assessment_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
