class CreateCourseAssessments < ActiveRecord::Migration
  def change
    create_table :course_assessments do |t|
      t.belongs_to :course
      t.belongs_to :assessment_type
      t.decimal :default_weight

      t.timestamps
    end
  end
end
