class CreateKataScheduleResources < ActiveRecord::Migration
  def change
    create_table :kata_schedule_resources do |t|
      t.references :kata_schedule, index: true
      t.references :resource, index: true

      t.timestamps
    end
  end
end
