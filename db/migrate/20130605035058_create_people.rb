class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.integer :external_id
      t.string :gender
      t.string :legacy_id

      t.timestamps
    end
  end
end
