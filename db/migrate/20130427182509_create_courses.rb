class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.references :course_type, index: true
      t.references :grading_type, index: true
      t.string :name
      t.string :number
      t.string :description
      t.string :level

      t.timestamps
    end
  end
end
