class CreateProgramGoals < ActiveRecord::Migration
  def change
    create_table :program_goals do |t|
      t.references :program, index: true
      t.string :description

      t.timestamps
    end
  end
end
