class CreateGradingTypes < ActiveRecord::Migration
  def change
    create_table :grading_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
