class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :name
      t.string :short_code
      t.text :description

      t.timestamps null: false
    end
    add_index :tracks, :name, unique: true
    add_index :tracks, :short_code, unique: true
  end
end
