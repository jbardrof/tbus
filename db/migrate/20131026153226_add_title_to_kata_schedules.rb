class AddTitleToKataSchedules < ActiveRecord::Migration
  def change
    add_column :kata_schedules, :title, :string
  end
end
