class CreateKataSchedules < ActiveRecord::Migration
  def change
    create_table :kata_schedules do |t|
      t.references :kata, index: true
      t.datetime :date
      t.integer :duration
      t.datetime :rescheduled
      t.boolean :canceled

      t.timestamps
    end
  end
end
