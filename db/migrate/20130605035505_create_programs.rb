class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.references :program_type, index: true
      t.references :organization, index: true
      t.string :name
      t.text :purpose, :limit => 4294967295

      t.timestamps
    end
  end
end
