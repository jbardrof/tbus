class AddProgramToUser < ActiveRecord::Migration
  def change
    add_reference :users, :program, index: true
    add_foreign_key :users, :programs
  end
end
