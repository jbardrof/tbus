class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
      t.references :role, index: true, foreign_key: true
      t.references :track, index: true, foreign_key: true
      t.string :email
      t.string :content

      t.timestamps null: false

      t.index [:role_id, :track_id], unique: true
    end

    
  end
end
