class AddPotentialToKataAssessments < ActiveRecord::Migration
  def change
    add_column :kata_assessments, :potential, :string
  end
end
