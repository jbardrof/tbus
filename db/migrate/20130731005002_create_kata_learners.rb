class CreateKataLearners < ActiveRecord::Migration
  def change
    create_table :kata_learners do |t|
      t.belongs_to :kata
      t.belongs_to :student
      t.string :final_assessment
      t.date :enrolled_on
      t.date :widthdrawl_on

      t.timestamps
    end
  end
end
