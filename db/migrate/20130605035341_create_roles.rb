class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.references :person, index: true
      t.references :organization, index: true
      t.string :type
      t.datetime :related_from
      t.datetime :related_thru

      t.timestamps
    end
  end
end
