class CreateUsEnrollmentsView < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      CREATE 
        ALGORITHM = UNDEFINED
        SQL SECURITY DEFINER
      VIEW `us_enrollments` AS
      SELECT 
        `r`.`id` AS `student_id`,
        `kl`.`kata_id` AS `kata_id`,
        `kl`.`id` AS `learner_id`,
        `r`.`person_id` AS `person_id`,
        `names`.`last_name` AS `last_name`,
        `names`.`first_name` AS `first_name`,
        `kl`.`final_assessment` AS `final_assessment`,
        `kl`.`enrolled_on` AS `enrolled_on`,
        `kl`.`widthdrawl_on` AS `widthdrawl_on`,
        `kl`.`created_at` AS `created_at`,
        `kl`.`updated_at` AS `updated_at`
      FROM
        ((`roles` `r`
        JOIN `us_names` `names` ON ((`names`.`contactable_id` = `r`.`person_id`)))
        JOIN `kata_learners` `kl` ON ((`r`.`id` = `kl`.`student_id`)))
     WHERE
        (`r`.`type` = 'Student')
    SQL
  end

  def self.down
    execute <<-SQL
      DROP VIEW us_enrollments
    SQL
  end
end
