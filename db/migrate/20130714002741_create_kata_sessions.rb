class CreateKataSessions < ActiveRecord::Migration
  def change
    create_table :kata_sessions do |t|
      t.references :program, index: true
      t.string :name
      t.string :description
      t.datetime :schedule_start
      t.datetime :schedule_until

      t.timestamps
    end
  end
end
