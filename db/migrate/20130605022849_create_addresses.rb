class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :contact_addresses do |t|
      t.references :contact, index: true
      t.string :name
      t.string :address_1
      t.string :address_2
      t.string :city
      t.string :state
      t.string :postal_code

      t.timestamps
    end
  end
end
