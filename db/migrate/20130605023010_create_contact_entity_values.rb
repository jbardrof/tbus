class CreateContactEntityValues < ActiveRecord::Migration
  def change
    create_table :contact_entity_values do |t|
      t.references :contact, index: true
      t.string :type
      t.string :name
      t.string :value

      t.timestamps
    end
  end
end
