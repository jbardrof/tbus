class CreateKataFacilitators < ActiveRecord::Migration
  def change
    create_table :kata_facilitators do |t|
      t.belongs_to :kata
      t.belongs_to :facilitator
      t.string :title

      t.timestamps
    end
  end
end
