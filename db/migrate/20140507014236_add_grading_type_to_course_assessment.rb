class AddGradingTypeToCourseAssessment < ActiveRecord::Migration
  def change
    add_column :course_assessments, :grading_type_id, :integer
    add_index :course_assessments, :grading_type_id  
  end
end
