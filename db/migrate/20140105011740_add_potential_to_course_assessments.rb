class AddPotentialToCourseAssessments < ActiveRecord::Migration
  def change
    add_column :course_assessments, :potential, :string
  end
end
