class AddGradingTypeToKataAssessment < ActiveRecord::Migration
  def change
    add_reference :kata_assessments, :grading_type, index: true
  end
end
