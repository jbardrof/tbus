class CreateKataLearnerAssessments < ActiveRecord::Migration
  def change
    create_table :kata_learner_assessments do |t|
      t.references :kata_learner, index: true
      t.references :kata_assessment, index: true
      t.string :grade
      t.boolean :on_time

      t.timestamps
    end
  end
end
