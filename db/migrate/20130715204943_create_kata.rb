class CreateKata < ActiveRecord::Migration
  def change
    create_table :kata do |t|
      t.references :kata_session, index: true
      t.references :course, index: true
      t.string :external_id
      t.string :name
      t.datetime :kata_start
      t.datetime :kata_until

      t.timestamps
    end
  end
end
