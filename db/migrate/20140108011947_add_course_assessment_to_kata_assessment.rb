class AddCourseAssessmentToKataAssessment < ActiveRecord::Migration
  def change
    add_column :kata_assessments, :course_assessment_id, :integer
    add_index :kata_assessments, :course_assessment_id
  end
end
