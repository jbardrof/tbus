class CreateCourseCatalogs < ActiveRecord::Migration
  def change
    create_table :course_catalogs do |t|
      t.belongs_to :course
      t.belongs_to :program
      t.timestamps
    end
  end
end
