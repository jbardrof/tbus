class CreateUsNameView < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      CREATE VIEW us_names as
      SELECT `cev1`.`id`, `cev1`.`contact_id`, `cnt`.`contactable_id`, `cnt`.`contactable_type`,
              `cev1`.`value` as last_name, `cev2`.`value` as first_name
        FROM `contact_entity_values` as cev1
              inner join `contact_entity_values` as cev2 on `cev1`.`contact_id` = `cev2`.contact_id
              inner join `contacts` as cnt on `cnt`.`id` = `cev1`.`contact_id`
       WHERE `cev1`.`name`  = 'lastname' and `cev2`.`name` = 'firstname';
    SQL
  end
  def self.down
    execute <<-SQL
      DROP VIEW us_names
    SQL
  end
end
