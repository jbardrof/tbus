class AddNameToCourseAssessments < ActiveRecord::Migration
  def change
    add_column :course_assessments, :name, :string
  end
end
