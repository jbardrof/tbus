# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171013012729) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "assessment_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "certificates", force: :cascade do |t|
    t.integer  "role_id",    limit: 4
    t.integer  "track_id",   limit: 4
    t.string   "email",      limit: 255
    t.string   "content",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "certificates", ["role_id", "track_id"], name: "index_certificates_on_role_id_and_track_id", unique: true, using: :btree
  add_index "certificates", ["role_id"], name: "index_certificates_on_role_id", using: :btree
  add_index "certificates", ["track_id"], name: "index_certificates_on_track_id", using: :btree

  create_table "contact_addresses", force: :cascade do |t|
    t.integer  "contact_id",  limit: 4
    t.string   "name",        limit: 255
    t.string   "address_1",   limit: 255
    t.string   "address_2",   limit: 255
    t.string   "city",        limit: 255
    t.string   "state",       limit: 255
    t.string   "postal_code", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "contact_addresses", ["contact_id"], name: "index_contact_addresses_on_contact_id", using: :btree

  create_table "contact_entity_values", force: :cascade do |t|
    t.integer  "contact_id", limit: 4
    t.string   "type",       limit: 255
    t.string   "name",       limit: 255
    t.string   "value",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "contact_entity_values", ["contact_id"], name: "index_contact_entity_values_on_contact_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.integer  "contactable_id",   limit: 4
    t.string   "contactable_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_assessments", force: :cascade do |t|
    t.integer  "course_id",          limit: 4
    t.integer  "assessment_type_id", limit: 4
    t.decimal  "default_weight",                 precision: 10
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",               limit: 255
    t.string   "potential",          limit: 255
    t.integer  "grading_type_id",    limit: 4
  end

  add_index "course_assessments", ["grading_type_id"], name: "index_course_assessments_on_grading_type_id", using: :btree

  create_table "course_catalogs", force: :cascade do |t|
    t.integer  "course_id",  limit: 4
    t.integer  "program_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courses", force: :cascade do |t|
    t.integer  "course_type_id",  limit: 4
    t.integer  "grading_type_id", limit: 4
    t.string   "name",            limit: 255
    t.string   "number",          limit: 255
    t.string   "description",     limit: 1000
    t.string   "level",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "track_id",        limit: 4
    t.integer  "status",          limit: 4,    default: 1, null: false
  end

  add_index "courses", ["course_type_id"], name: "index_courses_on_course_type_id", using: :btree
  add_index "courses", ["grading_type_id"], name: "index_courses_on_grading_type_id", using: :btree
  add_index "courses", ["track_id"], name: "index_courses_on_track_id", using: :btree

  create_table "grading_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kata", force: :cascade do |t|
    t.integer  "kata_session_id", limit: 4
    t.integer  "course_id",       limit: 4
    t.string   "external_id",     limit: 255
    t.string   "name",            limit: 255
    t.datetime "kata_start"
    t.datetime "kata_until"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "kata", ["course_id"], name: "index_kata_on_course_id", using: :btree
  add_index "kata", ["kata_session_id"], name: "index_kata_on_kata_session_id", using: :btree

  create_table "kata_assessments", force: :cascade do |t|
    t.integer  "kata_id",              limit: 4
    t.integer  "assessment_type_id",   limit: 4
    t.integer  "weight",               limit: 4
    t.string   "name",                 limit: 255
    t.integer  "kata_schedule_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "potential",            limit: 255
    t.integer  "course_assessment_id", limit: 4
    t.integer  "grading_type_id",      limit: 4
  end

  add_index "kata_assessments", ["assessment_type_id"], name: "index_kata_assessments_on_assessment_type_id", using: :btree
  add_index "kata_assessments", ["course_assessment_id"], name: "index_kata_assessments_on_course_assessment_id", using: :btree
  add_index "kata_assessments", ["grading_type_id"], name: "index_kata_assessments_on_grading_type_id", using: :btree
  add_index "kata_assessments", ["kata_id"], name: "index_kata_assessments_on_kata_id", using: :btree
  add_index "kata_assessments", ["kata_schedule_id"], name: "index_kata_assessments_on_kata_schedule_id", using: :btree

  create_table "kata_facilitators", force: :cascade do |t|
    t.integer  "kata_id",        limit: 4
    t.integer  "facilitator_id", limit: 4
    t.string   "title",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kata_learner_assessments", force: :cascade do |t|
    t.integer  "kata_learner_id",    limit: 4
    t.integer  "kata_assessment_id", limit: 4
    t.string   "grade",              limit: 255
    t.boolean  "on_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "kata_learner_assessments", ["kata_assessment_id"], name: "index_kata_learner_assessments_on_kata_assessment_id", using: :btree
  add_index "kata_learner_assessments", ["kata_learner_id"], name: "index_kata_learner_assessments_on_kata_learner_id", using: :btree

  create_table "kata_learner_participations", force: :cascade do |t|
    t.integer  "kata_learner_id",  limit: 4
    t.integer  "kata_schedule_id", limit: 4
    t.boolean  "on_time",                    default: true
    t.boolean  "participated"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "kata_learner_participations", ["kata_learner_id"], name: "index_kata_learner_participations_on_kata_learner_id", using: :btree
  add_index "kata_learner_participations", ["kata_schedule_id"], name: "index_kata_learner_participations_on_kata_schedule_id", using: :btree

  create_table "kata_learners", force: :cascade do |t|
    t.integer  "kata_id",          limit: 4
    t.integer  "student_id",       limit: 4
    t.string   "final_assessment", limit: 255
    t.date     "enrolled_on"
    t.date     "widthdrawl_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kata_schedule_resources", force: :cascade do |t|
    t.integer  "kata_schedule_id", limit: 4
    t.integer  "resource_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "kata_schedule_resources", ["kata_schedule_id"], name: "index_kata_schedule_resources_on_kata_schedule_id", using: :btree
  add_index "kata_schedule_resources", ["resource_id"], name: "index_kata_schedule_resources_on_resource_id", using: :btree

  create_table "kata_schedules", force: :cascade do |t|
    t.integer  "kata_id",     limit: 4
    t.datetime "date"
    t.integer  "duration",    limit: 4
    t.datetime "rescheduled"
    t.boolean  "canceled"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",       limit: 255
  end

  add_index "kata_schedules", ["kata_id"], name: "index_kata_schedules_on_kata_id", using: :btree

  create_table "kata_sessions", force: :cascade do |t|
    t.integer  "program_id",     limit: 4
    t.string   "name",           limit: 255
    t.string   "description",    limit: 255
    t.datetime "schedule_start"
    t.datetime "schedule_until"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "kata_sessions", ["program_id"], name: "index_kata_sessions_on_program_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.integer  "parent_id",      limit: 4
    t.string   "external_id",    limit: 255
    t.string   "federal_tax_id", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "people", force: :cascade do |t|
    t.integer  "external_id", limit: 4
    t.string   "gender",      limit: 255
    t.string   "legacy_id",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "program_goals", force: :cascade do |t|
    t.integer  "program_id",  limit: 4
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "program_goals", ["program_id"], name: "index_program_goals_on_program_id", using: :btree

  create_table "program_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "programs", force: :cascade do |t|
    t.integer  "program_type_id", limit: 4
    t.integer  "organization_id", limit: 4
    t.string   "name",            limit: 255
    t.text     "purpose",         limit: 4294967295
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "programs", ["organization_id"], name: "index_programs_on_organization_id", using: :btree
  add_index "programs", ["program_type_id"], name: "index_programs_on_program_type_id", using: :btree

  create_table "resources", force: :cascade do |t|
    t.string   "resource_type", limit: 255
    t.string   "description",   limit: 255
    t.string   "name",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: :cascade do |t|
    t.integer  "person_id",       limit: 4
    t.integer  "organization_id", limit: 4
    t.string   "type",            limit: 255
    t.datetime "related_from"
    t.datetime "related_thru"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["organization_id"], name: "index_roles_on_organization_id", using: :btree
  add_index "roles", ["person_id"], name: "index_roles_on_person_id", using: :btree

  create_table "tracks", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "short_code",  limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "tracks", ["name"], name: "index_tracks_on_name", unique: true, using: :btree
  add_index "tracks", ["short_code"], name: "index_tracks_on_short_code", unique: true, using: :btree

  create_table "us_enrollments", id: false, force: :cascade do |t|
    t.integer  "student_id",       limit: 4,   default: 0, null: false
    t.integer  "kata_id",          limit: 4,   default: 0, null: false
    t.integer  "course_id",        limit: 4,   default: 0, null: false
    t.integer  "track_id",         limit: 4
    t.integer  "learner_id",       limit: 4,   default: 0, null: false
    t.integer  "person_id",        limit: 4
    t.string   "last_name",        limit: 255
    t.string   "first_name",       limit: 255
    t.string   "final_assessment", limit: 255
    t.date     "enrolled_on"
    t.date     "widthdrawl_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "us_names", id: false, force: :cascade do |t|
    t.integer "id",               limit: 4,   default: 0, null: false
    t.integer "contact_id",       limit: 4
    t.integer "contactable_id",   limit: 4
    t.string  "contactable_type", limit: 255
    t.string  "last_name",        limit: 255
    t.string  "first_name",       limit: 255
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "invitation_token",       limit: 255
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit",       limit: 4
    t.integer  "invited_by_id",          limit: 4
    t.string   "invited_by_type",        limit: 255
    t.integer  "invitations_count",      limit: 4,   default: 0
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.integer  "program_id",             limit: 4
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["program_id"], name: "index_users_on_program_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "certificates", "roles"
  add_foreign_key "certificates", "tracks"
  add_foreign_key "courses", "tracks"
  add_foreign_key "users", "programs"
end
