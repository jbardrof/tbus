source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5.1'
# Use sqlite3 as the database for Active Record
gem 'mysql2'                            # The mysql2 connection gem, for databases.
gem 'paper_trail', '~> 3.0.0'           # Object versioning.
gem 'docile'                            # Ruby DSL assistance gem.
gem 'annotate'                          # Annotates ActiveRecord models
gem 'rails-i18n', '~> 4.0.0.pre'        # Rails-i18n provides translation and localization services for multiple languages.
gem 'nokogiri'                          # Nokogiri provides XML and HTML parsing
gem 'kaminari'                          # Paging support
gem 'sass-rails', '~> 5.0'              # Use SCSS for stylesheets
gem 'uglifier', '>= 1.3.0'              # Use Uglifier as compressor for JavaScript assets
gem 'coffee-rails', '~> 4.1.0'          # Use CoffeeScript for .coffee assets and views
gem 'therubyracer', platforms: :ruby    # See https://github.com/rails/execjs#readme for more supported runtimes

gem 'bootstrap', '~> 4.0.0.alpha3'
gem 'bootstrap_form', '~> 2.3.0'
gem 'font-awesome-rails', '~> 4.5.0'
gem 'jquery-rails'                      # Use jquery as the JavaScript library
gem 'turbolinks'                        # Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'jbuilder', '~> 2.0'                # Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'prawn', '~> 2.1.0'                 # PDF generation
gem 'prawn-table', '~> 0.2.0'           # Tabular data display support for Prawn

gem 'sdoc', '~> 0.4.0', group: :doc     # bundle exec rake doc:rails generates the API under doc/api.

gem 'activeadmin', '~> 1.0.0.pre2'      # Active admin, for managing all the administrative duties
gem 'active_skin', '~> 0.0.1'           # Themes active admin with a flatter theme
gem 'devise',      '~> 3.x'
gem 'devise_invitable', '~> 1.5.0'
gem 'admin_invitable'
gem 'haml', '~> 4.0.0'
gem 'syslog-logger', '~> 1.6.0'

source 'https://rails-assets.org' do
  gem 'rails-assets-tether', '>= 1.1.0'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'capistrano', '~> 3.4'        # Deployment Gem
  gem 'capistrano-rails', '~> 1.1'  # Rails extensions for the capistrano gem.
  gem 'capistrano-passenger', '~> 0.2'
  gem 'byebug'                    # Call 'byebug' anywhere in the code to stop execution and get a                  debugger console
end

group :test do
  gem 'capybara'
  gem 'connection_pool'
  gem 'launchy'
  gem 'minitest-reporters'
  gem 'mocha'
  gem 'poltergeist'
  gem 'shoulda-context'
  gem 'shoulda-matchers', '>= 3.0.1'
  gem 'test_after_commit'
end

group :development do
  gem 'web-console', '~> 2.0'     # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'guard', '>= 2.2.2', require: false
  gem 'guard-minitest', require: false
  gem 'rb-fsevent', require: false
  gem 'terminal-notifier-guard', require: false

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # gem 'better_errors'
  # gem "binding_of_caller"
  gem 'quiet_assets' # Disables some of the extra asset verbosity.
end
