module SessionsHelper
  def get_grade_for(learner, assessment)
    learner_assessment = learner.kata_learner_assessments.select{ |x| x.kata_assessment == assessment }

    unless learner_assessment.empty?
      return learner_assessment[0].grade
    else
      return nil
    end
  end

  def format_kata_time(time)
    return '' if time.nil?

    return time.strftime('%m-%d-%Y')
  end

  def is_on_time?(schedule, learner)
    participation = schedule.kata_learner_participations.select{ |x| x.kata_learner == learner }
    participation.first.nil? ? '' : participation.first.on_time
  end

  def grading_input_field(grading_type, current_grade)
    case grading_type.name.underscore.to_sym
    when :pass_fail
      check_box_tag 'grade', '1', (current_grade.to_i > 0 ? true: false)
    else
      text_field_tag 'grade', current_grade
    end
  end

  def print_name_tags(term, session)
    "javascript: w=window.open('#{name_tags_term_session_url(term, session)}'); w.print();"
  end
end
