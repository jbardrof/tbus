module ViewHelper
  def show_if_found(object, method)
    if object
      object.send(method)
    else
      'Not Found'
    end
  end
end
