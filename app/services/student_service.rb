
class StudentService
  # Localized name segments
  FIRST_NAME = 'FirstName'
  LAST_NAME = 'LastName'
  EMAIL_ADDRESS = 'Primary'

  def initialize(student_id = nil)
    if student_id
      @student = Student.joins(:person).where(id: student_id).first
    end
  end

  def find_by_person_id(person_id)
    @student = Student
      .includes(person: [contact: [:phones, :names, :addresses, :emails]])
      .where(person_id: person_id)
      .first
      #.joins(person: [contact: [:phones, :names, :addresses, :emails]])
  end

  def update_student(params)
    contact = @student.person.contact

    contact.names.select{ |x| x.name == 'FirstName' }.first.value = params['first_name']
    contact.names.select{ |x| x.name == 'LastName' }.first.value = params['last_name']
    contact.names.each(&:save)

    if contact.emails.first
      contact.emails.first.value = params['email_address']
    else
      contact.emails << ContactEmail.new(value: params['email_address'])
    end
    contact.emails.each(&:save)

    address = contact.addresses.first
    address.address_1 = params[:address_1]
    address.address_2 = params[:address_2]
    address.city = params[:city]
    address.state = params[:state]
    address.postal_code = params[:postal_code]
    address.save

    @student.save
  end

  def student
    @student
  end
end
