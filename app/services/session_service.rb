
class SessionService

  def update_session(params)
    Session.update(params[:id], sanitized_params(params))
  end

  private

    def sanitized_params(params)
      params.require(:session).permit(:name, :course_id, :kata_start, :kata_until)
    end
end
