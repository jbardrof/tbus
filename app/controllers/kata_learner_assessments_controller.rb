
class KataLearnerAssessmentsController < ApplicationController

  def create
    kata = Kata.find params[:kata_id]
    learner = kata.kata_learners.find params[:learner_id]
    assessment = kata.assessments.find params[:assessment_id]

    learner_assessment = KataLearnerAssessment.new(
      kata_assessment: assessment,
      kata_learner: learner,
      grade: params[:grade]
      )
    respond_to do |format|
      if learner_assessment.save
        format.json { render json: { success: true } }
      else
        format.json { render json: { success: false } }
      end
    end

  end

  def final_grade
    session = Session.find params[:session_id]
    learner = session.kata_learners.find params[:learner_id]

    respond_to do |format|
      if learner.update_attributes(final_assessment: params[:final_grade])
        format.json { render json: { success: true } }
      else
        format.json { render json: { success: false } }
      end
    end
  end
end
