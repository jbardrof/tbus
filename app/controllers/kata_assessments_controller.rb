
class KataAssessmentsController < ApplicationController

  def create
    @session = Session.find params[:session_id]
    @course_assessment = CourseAssessment.find params[:assessment]
    @kata_schedule = KataSchedule.find params[:schedule]

    @kata_assessment = KataAssessment.new(
      session: @session,
      assessment_type: @course_assessment.assessment_type,
      grading_type: @course_assessment.grading_type,
      potential: params[:potential],
      name: @course_assessment.name,
      kata_schedule: @kata_schedule,
      course_assessment: @course_assessment
    )

    respond_to do |format|
      if @kata_assessment.save
        format.json { render json: { success: true } }
      else
        format.json { render json: { success: false} }
      end
    end
  end
end
