
class CourseAssessmentsController < TbusBaseController
  before_action :init_controller

  def new
  end

  def edit
    @course_assessment = @course.assessments.find(params[:id])
  end

  def create
    @course = Course.find params[:course_id]

    @course_assessment = CourseAssessment.new(course_assessment_params)

    if @course_assessment.save
      @course.assessments << @course_assessment
      redirect_to controller: :courses, action: :show, id: @course.id
    else
      render :new
    end
  end

  def update
    @course_assessment = @course.assessments.find(params[:id])

    if @course_assessment.update_attributes(course_assessment_params)
      redirect_to controller: :courses, action: :show, id: @course.id
    else
      render :edit
    end
  end

  private
    def init_controller
      @course = Course.find params[:course_id]
      @course_assessment = CourseAssessment.new
      @assessment_types = AssessmentType.all
      @grading_types = GradingType.all
    end

    def course_assessment_params
      params.require(:course_assessment).permit(
        :course_id,
        :assessment_type_id,
        :grading_type_id,
        :name,
        :potential
      )
    end
end
