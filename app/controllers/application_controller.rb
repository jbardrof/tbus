class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  before_filter :set_locale

  def record_not_found
    flash[:notice] = 'The record requested could not be found'
    # Rather, redirect the user to the controller index where they attempted to go.
    redirect_to controller: 'core', action: 'index'
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale

    if !params[:locale].blank? and !I18n.available_locales.include?(params[:locale].to_sym)
      I18n.locale = I18n.default_locale
    end
  end
end
