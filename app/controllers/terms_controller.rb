
class TermsController < TbusBaseController
  before_action :set_term, only: [:show, :edit, :update, :destroy]

  # GET /terms
  # GET /terms.json
  def index
    @terms = Term.paged_with_programs(page: params[:page] || 1)

    respond_to do |format|
      format.html
      format.csv { send_data Term.includes(:program).to_csv,
        filename: "sessions-#{Date.today}.csv" }
    end
  end

  # GET /terms/new
  def new
    @term = Term.new

    # TODO: Programs will need to be filtered by organization
    @programs = Program.all
  end

  def show
    respond_to do |format|
      format.html
      format.csv { send_data @term.to_csv, filename: "session-#{@term.name}.csv" }
    end
  end

  # GET /terms/1/edit
  def edit
    @term = Term.find(params[:id])

    # TODO: Programs will need to be filtered by organization
    @programs = Program.all
  end

  # POST /terms
  # POST /terms.json
  def create
    parse_schedule_times

    @term = Term.new(term_params)

    respond_to do |format|
      if @term.save
        format.html { redirect_to @term, notice: 'Kata session was successfully created.' }
        format.json { render action: 'show', status: :created, location: @term }
      else
        format.html { render action: 'new' }
        format.json { render json: @term.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /terms/1
  # PATCH/PUT /terms/1.json
  def update
    parse_schedule_times

    respond_to do |format|
      if @term.update(term_params)
        format.html { redirect_to action: :show, id: @term.id, notice: 'Kata session was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @term.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /terms/1
  # DELETE /terms/1.json
  def destroy
    @term.destroy
    respond_to do |format|
      format.html { redirect_to terms_url }
      format.json { head :no_content }
    end
  end

  def new_session
    @term = Term.find(params[:id])
    # TODO: add program support.
    @available_courses = @current_program.courses

  end

  def create_session
    @term = Term.find(params[:id])

    params[:courses].each do |course_number|
      course = Course.where(number: course_number).first
      # TODO: add fault checking on the course

      Session.where(
        kata_session_id: @term.id,
        course_id: course.id,
        name: course.name
      ).first_or_create
    end

    redirect_to action: 'show', id: @term.id
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_term
      @term = Term.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def term_params
      params.require(:term).permit(:program_id, :name, :description, :schedule_start, :schedule_until)
    end

    def parse_schedule_times
      params[:term][:schedule_start] = Date.strptime params[:term][:schedule_start], '%m/%d/%Y'
      params[:term][:schedule_until] = Date.strptime params[:term][:schedule_until], '%m/%d/%Y'
    end

end
