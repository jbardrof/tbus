
class CoreController < TbusBaseController

  def index
    @terms = Term.term_summary(page: params[:page] || 1)

    @current_term = Term.order(schedule_start: :desc).first
  end

end
