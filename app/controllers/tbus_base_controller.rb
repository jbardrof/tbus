
class TbusBaseController < ApplicationController
  before_filter :authenticate_user!, :set_program!

  def set_program!
    @current_program ||= @current_user.program
  end
end
