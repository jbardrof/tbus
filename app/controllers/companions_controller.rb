
class CompanionsController < TbusBaseController
  before_filter :initialize_controller

  def index
    @companions = Person.companion_summary(page: params[:page] || 1)
  end

  def show
    @companion = Person.find_with_contact params[:id]
    @student = Student.work_with_tracks @companion.student.id
    @tracks = Track.all
  end

  def new
    @companion = Person.new
  end

  def create
    @companion = Person.new(companion_params)

    respond_to do |format|
      if @companion.save
        format.html { redirect_to companion_path(@companion.id),
          notice: 'Course was successfully created.' }
        format.json { render action: 'show', status: :created, location: @companion }
      else
        format.html { render action: 'new' }
        format.json { render json: @companion.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    student_service = StudentService.new
    student_service.find_by_person_id params[:id]

    respond_to do |format|
      if student_service.update_student(companion_params)
        format.html { redirect_to companion_path(student_service.student.person.id),
          notice: 'Course was successfully created.' }
        format.json { render action: 'show', status: :created, location: @companion }
      else
        format.html { render action: 'show' }
        format.json { render json: @companion.errors, status: :unprocessable_entity }
      end
    end
  end

  def transcript
    @companion = Person.find_with_contact params[:id]

    transcript = Documents::Transcript.new @companion

    respond_to do |format|
      format.pdf { send_data transcript.render, filename: 'summary_report.pdf',
        type: 'application/pdf', disposition: 'inline' }
    end
  end

  def search
    cev = ContactEntityValue.arel_table
    contact_entity_values = ContactEntityValue.where(cev[:value].matches("%#{params[:search]}%")).includes(:contact)
    contact_values = contact_entity_values.select { |e| e.contact.contactable_type == 'Person' }.map { |e| e.contact_id }
    contacts = Contact.where(id: contact_values)

    @companions = Person.where(id: contacts.map { |e| e.contactable_id })
                        .includes(:roles, contact: [:phones, :names, :addresses])
                        .page(params[:page] || 1).per(20)

    #@companions = Person.joins(:students)
    #People.joins(:city => {:state => :country})
    #.where(:country => {:id => Country.first.id}).all
    render action: :index
  end

  def start_merge
    @companion = Person.find_with_contact params[:id]

    
  end

  private

    def companion_params
      params.require(:person).permit(:last_name, :first_name, :email_address, :address_1,
        :address_2, :city, :state, :postal_code, :external_id)
    end

    def initialize_controller
      Person.send(:include, EnPerson)
    end
end
