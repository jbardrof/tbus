class SessionsController < ApplicationController
  layout :resolve_layout
  before_filter :initialize_controller

  # Session listing and creation are not implemented at the atomic level.
  def index; end
  def new; end
  def create; end

  def show
    @session = @term.sessions.includes(kata_learners: [:student, :kata_learner_assessments], schedules: [:kata_learner_participations])
      .find(params[:id])

    @learners = @session.kata_learners

    @enrollments = @session.enrollments.includes(:kata_learner).order(last_name: :asc, first_name: :asc)

    @schedule = @session.schedules.map { |e| {id: e.id, start: e.date.localtime , title: e.title, allDay: false } }

    @courses = Course.all.order(name: :asc)

    respond_to do |format|
      format.html
      format.csv { send_data @session.to_csv,
        filename: "session-#{Date.today}.csv" }
    end
  end

  def edit
    @session = Session.includes(:course).find(params[:id])

    @courses = Course.all.order(name: :asc)
  end

  def update
    @session = @session_service.update_session(params)

    respond_to do |format|
      unless @session.errors.any?
        format.html { redirect_to action: :show, id: @session.id, notice: 'kata updated' }
      else
        @courses = Course.all.order(name: :asc)
        format.html { render action: :edit }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @session_service.destroy(params)

      end
    end
  end

  def create_schedule
    params[:kata_schedule][:kata_id] = @term.sessions.find(params[:id]).id
    calc_session_date params

    respond_to do |format|
      if schedules = KataSchedule.new_recurring(kata_schedule_params)
        format.json { render json: { success: true, schedules: schedules  } }
      end
    end
  end

  def update_schedule
    @schedule = KataSchedule.where(id: params[:schedule_id], kata_id: params[:id]).first
    calc_session_date params

    respond_to do |format|
      if @schedule.update_attributes(kata_schedule_params)
        format.json { render json: { success: true, title: @schedule.title, start: @schedule.date.localtime  } }
      end
    end
  end

  def destroy_schedule
    @schedule = KataSchedule.where(id: params[:schedule_id], kata_id: params[:id]).first

    respond_to do |format|
      if @schedule.destroy
        format.json { render json: { success: true, id: @schedule.id }}
      end
    end
  end

  def synchronize_users
    # Get the kata requested
    @session = Session.find(params[:id])

    @session.synchronize_learners_from_ccb

    flash[:notice] = 'Synchronization Complete'
    redirect_to action: 'show', id: params[:id]
  end

  def attendance
    learner = KataLearner.find params[:learner]
    schedule = KataSchedule.find params[:schedule]

    participation = KataLearnerParticipation.where(
      kata_learner: learner,
      kata_schedule: schedule
    ).first_or_initialize()

    participation.participated = params[:selected]

    respond_to do |format|
      if participation.save
        format.json { render json: { success: :true } }
      end
    end
  end

  def tardy
    status = params[:status]
    learner = KataLearner.find params[:learner]
    schedule = KataSchedule.find params[:schedule]

    participation = KataLearnerParticipation.where(
      kata_learner: learner,
      kata_schedule: schedule
    ).first

    participation.on_time = (status == 'onTime') ? 1 : 0

    respond_to do |format|
      if participation.save
        format.json { render json: { success: :true } }
      end
    end
  end

  def transcript
    @session = Session.find params[:id]

    send_data @session.create_transcript, filename: "#{@session.name.underscore}-#{Date.today}.csv"
  end

  def assign_to_course
    @session = Session.find params[:id]
    course = Course.find params[:session][:course_id]
    @session.course_id = course.id

    respond_to do |format|
      if @session.save
        format.html { redirect_to action: :show, id: @session.id }
      end
    end
  end

  def name_tags
    @session = Session.find params[:id]

    @kata_learners = @session.kata_learners.includes(student: [:proper_name])
  end

  private

    def initialize_controller
      @session_service ||= SessionService.new

      @term = Term.find(params[:term_id])
    end

    def calc_session_date(params)
      hour, minute = params[:time].split(':')

      hour = if params[:meridian].downcase == 'pm' and hour.to_i != 12
        hour.to_i + 12
      elsif params[:meridian].downcase == 'am' and hour.to_i == 12
        0
      else
        hour.to_i
      end

      raw_date = Time.parse params[:date]

      params[:kata_schedule][:date] = Time.new(raw_date.year, raw_date.month, raw_date.day, hour, minute.to_i, 0)

      return params
    end

    def kata_schedule_params
      params.require(:kata_schedule).permit(:kata_id, :title, :date, :duration, :repeats)
    end

    # Some items will not be following the default layout, we can customize it here.
    def resolve_layout
      case action_name
      when 'name_tags'
        'print_basic' # the basic print layout affords some styles, no chrome
      else
        'application'
      end
    end
end
