class CoursesController < TbusBaseController
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  # GET /courses
  # GET /courses.json
  def index

    @courses = Course.all.order('name asc')

    respond_to do |format|
      format.html
      format.csv { send_data @courses.all_to_csv, filename: "courses-#{Date.today}.csv" }
    end
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
    respond_to do |format|
      format.html
      format.csv { send_data @course.to_csv, filename: "course-#{@course.number}.csv" }
    end
  end

  # GET /courses/new
  def new
    @course = Course.new
    @tracks = Track.all

    # Currently we'll bring all programs back, eventually filter by permissions
    @programs = Program.all
  end

  # GET /courses/1/edit
  def edit
    # Currently we'll bring all programs back, eventually filter by permissions
    @programs = Program.all

    @tracks = Track.all
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)

    respond_to do |format|
      if @course.save
        format.html { redirect_to @course, notice: 'Course was successfully created.' }
        format.json { render action: 'show', status: :created, location: @course }
      else
        # Currently we'll bring all programs back, eventually filter by permissions
        @programs = Program.all
        @tracks = Track.all
        format.html { render action: 'new' }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to @course, notice: 'Course was successfully updated.' }
        format.json { head :no_content }
      else
        @programs = Program.all
        @tracks = Track.all
        format.html { render action: 'edit' }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.archived!

    respond_to do |format|
      format.html { redirect_to courses_url }
      format.json { head :no_content }
    end
  end

  private

    # User callbacks to share coommon setup or constraints between actions.
    def set_course
      @course = Course.find params[:id]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:name, :number, :description, :program_ids, :track_id)
    end
end
