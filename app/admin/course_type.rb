ActiveAdmin.register CourseType do
  permit_params :name, :description
end
