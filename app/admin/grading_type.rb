ActiveAdmin.register GradingType do
  permit_params :name, :description
end
