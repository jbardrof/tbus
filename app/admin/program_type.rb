ActiveAdmin.register ProgramType do
  permit_params :name, :description
end
