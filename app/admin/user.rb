ActiveAdmin.register User do

  index do
    # set the columns to show, there are too many to show them all
    selectable_column
    column :first_name
    column :last_name
    column :email
    column :created_at
    actions
  end

  # declare the filters available
  filter :email
  filter :first_name
  filter :last_name
  filter :created_at

  # declare the params that are able to be edited here.
  permit_params :program_id, :email, :first_name, :last_name

  action_item :invite do
    link_to 'Invite New User', new_invitation_admin_users_path
  end

  form do |f|
    inputs do
      input :program
      input :email
      input :first_name
      input :last_name
    end
    actions
  end

  collection_action :new_invitation do
  	@user = User.new
  end

  collection_action :send_invitation, :method => :post do
    # Set the permitted params for invitation on the user object.
    # this usually is done in the controller but for the purposes of active admin
    # this is the controller action
    user_params = params.require(:user).permit(:email, :first_name, :last_name)

    # Send the user invitation
  	@user = User.invite!(user_params, current_user)

    # If there are no errors in the invitation process, redirect to the users page.
  	if @user.errors.empty?
  		flash[:success] = "User has been successfully invited."
  		redirect_to admin_users_path
  	else
  		messages = @user.errors.full_messages.map { |msg| msg }.join
  		flash[:error] = "Error: " + messages
  		redirect_to new_invitation_admin_users_path
  	end
  end

end
