ActiveAdmin.register AssessmentType do
  permit_params :name, :description
end
