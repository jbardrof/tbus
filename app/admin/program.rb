ActiveAdmin.register Program do
  permit_params :name, :purpose, :organization_id, :program_type_id
end
