ActiveAdmin.register Track do
  permit_params :name, :short_code, :description
end