# == Schema Information
#
# Table name: roles
#
#  id              :integer          not null, primary key
#  person_id       :integer
#  organization_id :integer
#  type            :string(255)
#  related_from    :datetime
#  related_thru    :datetime
#  created_at      :datetime
#  updated_at      :datetime
#

class Facilitator < Role
  has_many :kata_facilitators
  has_many :katas, through: :kata_facilitators

  def self.create_from_ccb(ccb_individual)
    person = Person.find_or_create_by external_id: ccb_individual.id

    person.contact ||= Contact.new
    person.contact.names << ContactName.create(name: 'FirstName', value: ccb_individual.first_name)
    person.contact.names << ContactName.create(name: 'LastName', value: ccb_individual.last_name)
    # person.contact.phones << ContactPhone.create(name: 'Home', value: ccb_individual.first_name)
    # person.contact.addresses << ContactAddress.create(name: 'Home', address_1: ccb_individual.first_name, city: ccb_individual.first_name,
    #  state: ccb_individual.first_name, postal_code: ccb_individual.first_name)

    new_facilitator = create(related_from: Time.now)
    person.roles << new_facilitator

    new_facilitator
  end

  def courses_managed
    katas.map(&:course)
  end
end
