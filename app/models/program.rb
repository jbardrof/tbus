# == Schema Information
#
# Table name: programs
#
#  id              :integer          not null, primary key
#  program_type_id :integer
#  organization_id :integer
#  name            :string(255)
#  purpose         :text(2147483647)
#  created_at      :datetime
#  updated_at      :datetime
#

class Program < ActiveRecord::Base
  belongs_to :program_type
  belongs_to :organization

  has_many :courses, through: :course_catalogs
  has_many :course_catalogs
  has_many :terms

  def to_s
    self.name
  end
end
