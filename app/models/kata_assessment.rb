# == Schema Information
#
# Table name: kata_assessments
#
#  id                   :integer          not null, primary key
#  kata_id              :integer
#  assessment_type_id   :integer
#  weight               :integer
#  name                 :string(255)
#  kata_schedule_id     :integer
#  created_at           :datetime
#  updated_at           :datetime
#  potential            :string(255)
#  course_assessment_id :integer
#

class KataAssessment < ActiveRecord::Base
  belongs_to :session, foreign_key: 'kata_id'
  belongs_to :assessment_type
  belongs_to :kata_schedule
  belongs_to :course_assessment
  belongs_to :grading_type

  has_many :kata_learner_assessments
end
