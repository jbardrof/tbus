# == Schema Information
#
# Table name: contact_addresses
#
#  id          :integer          not null, primary key
#  contact_id  :integer
#  name        :string(255)
#  address_1   :string(255)
#  address_2   :string(255)
#  city        :string(255)
#  state       :string(255)
#  postal_code :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class ContactAddress < ActiveRecord::Base
  belongs_to :contact

  validates :name, length: { maximum: 255 }
  validates :address_1, length: { in: 1..255 }
  validates :address_2, length: { maximum: 255 }
  validates :city, length: { in: 1..255 }, format: { with: /\A[a-zA-Z\s]+\z/}
  validates :state, length: { in: 1..255 }, format: { with: /\A[a-zA-Z\s]+\z/}
  validates :postal_code, length: { minimum: 5, maximum: 9 }
end
