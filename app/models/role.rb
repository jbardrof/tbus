# == Schema Information
#
# Table name: roles
#
#  id              :integer          not null, primary key
#  person_id       :integer
#  organization_id :integer
#  type            :string(255)
#  related_from    :datetime
#  related_thru    :datetime
#  created_at      :datetime
#  updated_at      :datetime
#

class Role < ActiveRecord::Base
  belongs_to :person
  belongs_to :organization
  has_many :learners, class_name: 'KataLearner', foreign_key: 'student_id'
end
# companion = Person.includes({:contact => [:addresses, :names]}, {:roles => [:learners]}).find(172)
