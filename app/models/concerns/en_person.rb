module EnPerson
  extend ActiveSupport::Concern

  # Localized name segments
  FIRST_NAME = 'FirstName'
  LAST_NAME = 'LastName'
  EMAIL_ADDRESS = 'Primary'

  attr_accessor :first_name, :last_name, :address_1, :address_2,
    :city, :state, :postal_code, :email_address

  def save_localized_contact
    # Create the persons contact record if we do not yet have one.
    self.contact ||= Contact.create
    # Add the first name to the persons contact record
    first_name = ContactName.new(name: FIRST_NAME, value: self.first_name)

    if first_name.valid?
      self.contact.names << first_name
    else
      first_name.errors.messages.each { |e| self.errors.add(:first_name, e) }
    end

    # Add the last name to the persons contact record
    last_name = ContactName.new(name: LAST_NAME, value: self.last_name)

    if last_name.valid?
      self.contact.names << last_name
    else
      last_name.errors.messages.each { |e| self.errors.add(:last_name, e) }
    end

    # Add the email address of record
    email_address = ContactEmail.new(name: EMAIL_ADDRESS, value: self.email_address)

    if email_address.valid?
      self.contact.emails << email_address
    else
      email_address.errors.messages.each { |e| self.errors.add(:email_address, e) }
    end

    # Add an address to the persons contact record
    address = ContactAddress.new(
      address_1:   self.address_1,
      address_2:   self.address_2,
      city:        self.city,
      state:       self.state,
      postal_code: self.postal_code
    )
    if address.valid?
      self.contact.addresses << address
    else
      address.errors.messages.each { |k, v| self.errors.add(k,v) }
    end

    if self.errors.any?
      raise ActiveRecord::RecordInvalid.new(self)
    else

    end
  end

  def update_with_contact(params)
    # This is a bit dirty, we're doing update and replacements here.
    # TODO figure out a better way to update than procedurally doing it here.
  end

  module ClassMethods
    # Returns a person but with the localized contact information attached
    # as native attributes to the class itself.
    def find_with_contact(id)
      person = Person.find(id)

      # Populate the first and last name
      person.first_name = person.proper_name.first_name
      person.last_name = person.proper_name.last_name

      # Populate the default email address.
      person.email_address = person.contact.try(:emails).try(:first).try(:value)

      # Populate the address
      default_address = person.contact.addresses.first
      person.address_1 = default_address&.address_1
      person.address_2 = default_address&.address_2
      person.city = default_address&.city
      person.state = default_address&.state
      person.postal_code = default_address&.postal_code

      return person
    end
  end

  def self.included(base)
    base.extend ClassMethods
  end
end
