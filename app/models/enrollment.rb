class Enrollment < ActiveRecord::Base
  self.table_name = 'us_enrollments'

  belongs_to :kata_learner, foreign_key: 'learner_id'
end