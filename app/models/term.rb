# == Schema Information
#
# Table name: kata_sessions
#
#  id             :integer          not null, primary key
#  program_id     :integer
#  name           :string(255)
#  description    :string(255)
#  schedule_start :datetime
#  schedule_until :datetime
#  created_at     :datetime
#  updated_at     :datetime
#

class Term < ActiveRecord::Base
  self.table_name = 'kata_sessions'

  belongs_to :program

  has_many :sessions, foreign_key: 'kata_session_id'

  scope :term_summary, -> (options) {
    select(:id, :name, :schedule_start)
    .order('schedule_start desc')
    .page(options[:page])
    .per(10)
  }

  scope :paged_with_programs, -> (options) {
    order('schedule_start DESC')
    .includes(:program)
    .page(options[:page])
    .per(25)
  }

  scope :current_term, -> { limit(1).order('schedule_start desc').first }

  def self.to_csv
    attributes = %w{name program description schedule_start schedule_until}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |course|
        csv << attributes.map { |attr| course.send(attr) }
      end
    end
  end

  def to_csv
    CSV.generate(headers: true) do |csv|
      csv << %w{program name description schedule_start schedule_until}
      csv << [self.program.name, self.name, self.description, self.schedule_start,
        self.schedule_until]
      csv << []
      csv << %w{class_name course_number}
      self.sessions.each do |session|
        csv << [session.name, session.try(:course).try(:number)]
      end
    end
  end
end
