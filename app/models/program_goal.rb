# == Schema Information
#
# Table name: program_goals
#
#  id          :integer          not null, primary key
#  program_id  :integer
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class ProgramGoal < ActiveRecord::Base
  belongs_to :program
end
