# == Schema Information
#
# Table name: people
#
#  id          :integer          not null, primary key
#  external_id :integer
#  gender      :string(255)
#  legacy_id   :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Person < ActiveRecord::Base
  # Relationships
  has_one :contact, as: :contactable, foreign_key: 'contactable_id'
  has_one :student, class_name: 'Student', foreign_key: 'person_id'
  has_many :roles, foreign_key: 'person_id'
  has_one :proper_name, foreign_key: 'contactable_id'

  # Callback Registrations
  after_save :save_localized_contact

  # Scopes
  scope :companion_summary, -> (options) {
    joins(:proper_name)
    .includes(:roles, contact: [:phones, :names, :addresses])
    .order('last_name asc, first_name asc')
    .page(options[:page])
    .per(20)
  }

  def self.create_or_update_from_ccb(ccb_individual)
    person = find_or_create_by external_id: ccb_individual.id

    person.contact ||= Contact.new

    person.contact.emails = [ContactEmail.create(name: 'Primary', value: ccb_individual.email)]

    person.contact.names = [ContactName.create(name: 'FirstName', value: ccb_individual.first_name),
                            ContactName.create(name: 'LastName', value: ccb_individual.last_name)]

    person.contact.phones = ccb_individual.ccb_phones.map { |e| ContactPhone.create(name: e.type, value: e.value) }

    person.contact.addresses = ccb_individual.ccb_addresses.map do |addr|
      ContactAddress.create(
        name:         addr.type,
        address_1:    addr.street_address,
        city:         addr.city,
        state:        addr.state,
        postal_code:  addr.zip)
    end

    person
  end

  private
    def save_localized_contact
      # STUB for implementation under localized saves.
    end
end
