# == Schema Information
#
# Table name: kata_facilitators
#
#  id             :integer          not null, primary key
#  kata_id        :integer
#  facilitator_id :integer
#  title          :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#

class KataFacilitator < ActiveRecord::Base
  belongs_to :kata
  belongs_to :facilitator
end
