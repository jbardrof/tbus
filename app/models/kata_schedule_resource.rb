# == Schema Information
#
# Table name: kata_schedule_resources
#
#  id               :integer          not null, primary key
#  kata_schedule_id :integer
#  resource_id      :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class KataScheduleResource < ActiveRecord::Base
  belongs_to :kata_schedule
  belongs_to :resource
end
