# == Schema Information
#
# Table name: course_catalogs
#
#  id         :integer          not null, primary key
#  course_id  :integer
#  program_id :integer
#  created_at :datetime
#  updated_at :datetime
#

class CourseCatalog < ActiveRecord::Base
  belongs_to :course
  belongs_to :program
end
