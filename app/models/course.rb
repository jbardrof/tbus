# == Schema Information
#
# Table name: courses
#
#  id              :integer          not null, primary key
#  course_type_id  :integer
#  grading_type_id :integer
#  name            :string(255)
#  number          :string(255)
#  description     :string(255)
#  level           :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class Course < ActiveRecord::Base
  belongs_to :course_type
  belongs_to :grading_type
  belongs_to :track
  
  has_many :assessment_types, through: :course_assessments
  has_many :assessments, class_name: 'CourseAssessment'
  has_many :course_catalogs
  has_many :programs, through: :course_catalogs

  validates :name, uniqueness: true, length: { in: 1..254 }
  validates :number, uniqueness: true, length: { in: 1..254 }

  enum status: [:archived, :active, :upcoming]

  def to_csv
    CSV.generate do |csv|
      csv << %w{name number description created_at}
      csv << [self.name, self.number, self.description, self.created_at]
      csv << []
      csv << %w{assessment_name type potential}
      self.assessments.each do |assessment|
        csv << [assessment.name, assessment.assessment_type.name, assessment.potential]
      end
    end
  end

  def self.all_to_csv
    attributes = %w{name number description created_at}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |course|
        csv << attributes.map { |attr| course.send(attr) }
      end
    end
  end
end
