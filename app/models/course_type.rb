# == Schema Information
#
# Table name: course_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class CourseType < ActiveRecord::Base
end
