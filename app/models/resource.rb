# == Schema Information
#
# Table name: resources
#
#  id            :integer          not null, primary key
#  resource_type :string(255)
#  description   :string(255)
#  name          :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Resource < ActiveRecord::Base
  has_many :kata_schedule_resources
  has_many :kata_schedules, through: :kata_schedule_resources
end
