# == Schema Information
#
# Table name: contacts
#
#  id               :integer          not null, primary key
#  contactable_id   :integer
#  contactable_type :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class Contact < ActiveRecord::Base
  belongs_to :contactable, polymorphic: true

  has_many :phones,    class_name: 'ContactPhone',   foreign_key: 'contact_id', dependent: :destroy
  has_many :names,     class_name: 'ContactName',    foreign_key: 'contact_id', dependent: :destroy
  has_many :addresses, class_name: 'ContactAddress', foreign_key: 'contact_id', dependent: :destroy
  has_many :emails,    class_name: 'ContactEmail',   foreign_key: 'contact_id', dependent: :destroy
end
