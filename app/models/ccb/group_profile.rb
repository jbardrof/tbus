
class Ccb::GroupProfile < Ccb::Base
  self.service            = 'group_profiles'
  self.attributes         = [:name, :group_type]
  self.object_node        = :groups
  self.object_root        = :group
  self.api_response_nodes = %w[groups group]

  attr_accessor :id, :name, :group_type, :description, :modified, :main_leader,
                :department, :inactive

  def inactive?
    # CCB uses strings for true/false, this gives us a more rails way.
    self.inactive == 'true'
  end

  class << self
    def create(options = {})
      @service = 'create_group'
      super options
    end

    def where(options = {})
      @service = 'group_profiles'
      super options
    end

    def find(id)
      @service = 'group_profile_from_id'
      super id: id
    end

    def all
      @service = 'group_profiles'
      super {}
    end

    def create_group_from_kata(kata)
      create({
        name:        "#{kata.kata_session.name} - #{kata.course.name}",
        description: kata.course.description.gsub!(/\'/, %q(\\\'))
      })
    end

    def find_all_groups_by_name(name)
      groups = self.all

      groups.select! { |e| e.name.include? name }
    end
  end
end
