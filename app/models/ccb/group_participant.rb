
class Ccb::GroupParticipant < Ccb::Base
  self.service            = 'group_participants'
  self.attributes         = [:name, :group_type]
  self.object_node        = :group
  self.object_root        = :participants
  self.api_response_nodes = %w[groups group participants participant]

  attr_accessor :id, :name, :status, :modifier, :created, :modified

  class << self
    def create(options = {})
      raise NotImplementedError
      @service = 'create_group'
      super options
    end

    def where(options = {})
      @service = 'group_participants'
      super options
    end

    def find(id)
      @service = 'group_participants'
      super id: id
    end
  end
end
