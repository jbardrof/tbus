class Ccb::Phone < Ccb::Base

  attr_accessor :type, :value

  self.api_response_nodes = %w[phones phone]
end
