
class Ccb::Address < Ccb::Base
  attr_accessor :type, :street_address, :city,
    :state, :zip, :country, :line_1, :line_2

  self.api_response_nodes = %w[addresses address]
end
