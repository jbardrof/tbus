
class Ccb::Individual < Ccb::Base
  self.service            = 'individual_profile_from_id'
  self.api_response_nodes = %w[ individuals individual ]

  has_children :addresses
  has_children :phones

  attr_accessor :id, :giving_number, :family, :first_name, :last_name, :email

  class << self
    def create(options = {})
      raise NotImplementedError
      @service = 'create_group'
      super options
    end

    def where(options = {})
      raise NotImplementedError
      @service = 'group_participants'
      super options
    end

    def find(id)
      @service = 'individual_profile_from_id'
      super individual_id: id
    end
  end
end
