
module Ccb::Relations
  module ClassMethods
    def has_children(name)
      @children ||= []
      @children << "Ccb::#{name.to_s.singularize.classify}".constantize

      @child_collection ||= []
      @child_collection << name
    end

    def child_collection
      @child_collection
    end

    def has_children?
      return !@children.nil?
    end

    def relations
      @children
    end
  end

  def self.included(base)
    base.extend(ClassMethods)
  end
end

class Ccb::Base
  extend ActiveModel::Naming
  include ActiveModel::AttributeMethods
  include Ccb::Relations
  require 'uri'
  require 'net/http'

  attr_accessor :service


  def initialize(options = {})
    @service = self.class.service

    # Any options sent to the class need to be populated into the instance properties if the property exists.
    # This property array is what is then used to translate into the render type.
    @props = []
    options.each do |opt, val|
      # Check if the property has been defined, if it has, set it.
      if self.respond_to?(opt)
        self.send("#{opt.to_s}=", val)
        @props << opt.to_s
      end
    end

    self.class.child_collection.each do |name|
      create_attr name
    end if self.class.has_children?

    @new_record = !self.respond_to?('id') || self.id.nil?
  end

  def save

  end

  def create_method(name, &block)
    self.class.send(:define_method, name, &block)
  end

  def create_attr(name)
    create_method name.to_sym do
      instance_variable_get "@#{name}"
    end

    create_method "#{name}=".to_sym do |val|
      instance_variable_set "@#{name}", val
    end

  end

  def to_key
  end

  def persisted?
    !@new_record
  end

  class << self
    attr_accessor :object_root, :service, :object_node, :attributes, :api_response_nodes

    def init_api
      @@base_url = Tbus::Application.config.ccb_base_url
      @@username = Tbus::Application.config.ccb_username
      @@password = Tbus::Application.config.ccb_password

      @@response_base = (['ccb_api','response'] << api_response_nodes).join('/')
    end

    def all
      response = send_request(Net::HTTP::Get)

      response.body.blank? ? false : build_response_from(response)
    end

    def where(options = {})
      response = send_request Net::HTTP::Get, query: options

      unless response.body.blank?
        return build_response_from response
      end

      return false
    end

    def find(id = {})
      response = send_request Net::HTTP::Get, query: id

      unless response.body.blank?
        return build_response_from response
      end

      return false
    end

    def build_response_from(response)
      object_map = Nokogiri::XML response.body

      objects = object_map.xpath @@response_base

      result_array = []
      objects.each do |o|
        object_instance = self.ancestors[0].new(build_params(o, self.ancestors[0]))

        if has_children?
          @children.each_with_index do |child, index|
            child_objects = o.xpath (child.api_response_nodes.join('/'))

            child_collection = []
            child_objects.each do |co|
              child_object = child.new(build_params(co, child))
              child_collection << child_object

            end
            object_instance.send("#{@child_collection[index]}=", child_collection)
          end
        end

        result_array << object_instance
      end

      return result_array
    end

    def create(form_params)
      build_response_from send_request Net::HTTP::Post, body: form_params, query: ''
    end

    private

    # This method will provide the actual calls to the api, this is the communication layer.
    def send_request(request_type, options = {})
      # Do the initialization tasks.
      init_api

      # Track whether or not the api has fired an exception.
      api_exception = false

      begin
        response = nil
        request = nil

        # Build the CCB uri
        uri = URI.parse @@base_url

        #Time the request
        result_time = Timer::clock_me do
          # Build the request object, this will take the path and other meta data required
          request = request_type.new "#{uri.path}#{build_query_from(options[:query])}"
          request.basic_auth @@username, @@password
          request.body = build_body_from(options[:body]) if options[:body]

          https = Net::HTTP.new(uri.host, uri.port)
          https.use_ssl = true
          https.ssl_version = 'TLSv1_2'
          response = https.request(request)

          # Perform the request and load the result into an accesisble request object
          #response = Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
          #  http.verify_mode = OpenSSL::SSL::VERIFY_PEER
          #  http.ssl_version = :TLSv1_
          #  http.request(request)
          #end

        end

      # Rescue from any possible Net::HTTP errors. We don't want the application to crash if the api is down
      # Instead log the exception, return false and let the application decide what to do with a failed api call.
      rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, EOFError, Errno::ECONNREFUSED,
             Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
        api_exception = { error: e }
        throw e

      # Ensure we return a positive result. If the api did not return successfully false means we had problems.
      ensure
        unless response == nil 
          Clogger.rest_log request_type: request.method, url: "Result: #{response.code} - #{request.path} \n #{response.body}", time: result_time, result: response.code
          return api_exception ? false : response
        end
      end
    end

    def build_query_from(query)
      url = unless query.nil? || query.empty?
        q_ary = query.collect { |k,v| "#{k}=#{v}" }
        "?srv=#{@service}&#{q_ary.join '&'}"
      else
        "?srv=#{@service}"
      end
      return url
    end

    def build_body_from(query)
      q_ary = query.collect { |k,v| "#{k}=#{v}" }
      return URI::encode(q_ary.join '&')
    end


    # The param builder will iterate through the objects methods and populate them with
    # matching elements and attributes from the xml result set.
    def build_params(result, object)
      method_hash = {}

      # Remove all the 'setter' methods from the method collection and iterate through the remaining methods.
      object.instance_methods(false).delete_if{ |x| x.match(/\=|\?/) }.each.each do |m|

        method_hash[m] = if m.to_s == 'value'
          result.text
        elsif !result.xpath(m.to_s).text.empty?       # first check if there is a matching
          result.xpath(m.to_s).text                                 # element in the node for the requested property
        elsif result.xpath("@#{m.to_s}")[0]                         # If no element is found, check for an attribute,
          result.xpath("@#{m.to_s}")[0].text                        # if an attribute is found, add it.
        else                                                        # If nothing is found, set the value to nil.
          nil
        end
      end

      return method_hash
    end
  end
end
