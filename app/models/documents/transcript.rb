
class Documents::Transcript < Documents::PdfBase
  def initialize(companion = nil)
    @companion = companion
    super()

    header

    body
  end

  def body
    text "Transcript for: #{@companion.proper_name.first_name} #{ @companion.proper_name.last_name}"

    text "Prepared on: #{I18n.l(Date.today, format: :long)}"

    move_down 10

    text I18n.t 'documents.transcripts.greeting', name: @companion.proper_name.first_name

    move_down 10

    text I18n.t 'documents.transcripts.description'

    move_down 20

    stroke_horizontal_rule

    move_down 10

    table transcript_items, column_widths: [75, 240, 125, 100] do
      cells.borders = []
      cells.padding = 5

      row(0).borders = [:bottom]
      row(0).border_width = 2
      row(0).font_style = :bold
    end

  end

  def transcript_items
    result = []
    result << ['Class ID', 'Class Name', 'Term', 'Grade']
    @companion.student.kata_learners.each do |m|
      result << [
        m.session.course.number,
        m.session.name,
        m.session.term.name,
        m.final_assessment ]
    end

    return result
  end
end
