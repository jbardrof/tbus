
module Documents
  class PdfBase < Prawn::Document

    def initialize(default_prawn_options = {})
      super(default_prawn_options)

      font_size 10
    end

    def header(title = nil)
      text 'Abundant Life Institute', size: 18, style: :bold

      move_down 10

      stroke_horizontal_rule

      move_down 20
    end
  end
end
