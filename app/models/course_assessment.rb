# == Schema Information
#
# Table name: course_assessments
#
#  id                 :integer          not null, primary key
#  course_id          :integer
#  assessment_type_id :integer
#  default_weight     :integer
#  created_at         :datetime
#  updated_at         :datetime
#  name               :string(255)
#  potential          :string(255)
#

class CourseAssessment < ActiveRecord::Base
  belongs_to :course
  belongs_to :assessment_type
  belongs_to :grading_type

  # TODO: validate name uniqueness per course
  validates :name, length: { in: 3..100 }
  validates :potential, format: { with: /\A[1-9][0-9]*\z/} # Regex: Allow any whole number not starting with 0.
  # validates :default_weight, numericality: { greater_than: 0, less_than_or_equal_to: 100 }
  validates :assessment_type_id, presence: true

  # For now, default ordering for course assessments, type then name
  default_scope { order('assessment_type_id asc, name asc') }
end
