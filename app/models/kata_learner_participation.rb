# == Schema Information
#
# Table name: kata_learner_participations
#
#  id               :integer          not null, primary key
#  kata_learner_id  :integer
#  kata_schedule_id :integer
#  on_time          :boolean          default(TRUE)
#  participated     :boolean
#  created_at       :datetime
#  updated_at       :datetime
#

class KataLearnerParticipation < ActiveRecord::Base
  belongs_to :kata_learner
  belongs_to :kata_schedule
end
