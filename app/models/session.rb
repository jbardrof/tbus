# == Schema Information
#
# Table name: kata
#
#  id              :integer          not null, primary key
#  kata_session_id :integer
#  course_id       :integer
#  external_id     :string(255)
#  name            :string(255)
#  kata_start      :datetime
#  kata_until      :datetime
#  created_at      :datetime
#  updated_at      :datetime
#

class Session < ActiveRecord::Base
  self.table_name = 'kata'

  belongs_to :term, foreign_key: 'kata_session_id'
  belongs_to :course

  has_many :kata_learners, foreign_key: 'kata_id'
  has_many :students, through: :kata_learners
  has_many :kata_facilitators, foreign_key: 'kata_id'
  has_many :facilitators, through: :kata_facilitators
  has_many :assessments, class_name: 'KataAssessment', foreign_key: 'kata_id'
  has_many :schedules, class_name: 'KataSchedule', foreign_key: 'kata_id'
  has_many :enrollments, foreign_key: 'kata_id'

  def has_assessment?(course_assessment)
    !assessments.where(course_assessment: course_assessment).empty?
  end

  def assigned_assessment(course_assessment)
    assessments.where(course_assessment: course_assessment).first
  end

  def synchronize_learners_from_ccb
    # Retrieve the Participants from CCB. Utilizes the CCB api for group participants.
    ccb_participants = Ccb::GroupParticipant.find(self.external_id)

    # get a collection of student ID's from the resulting API call of group participants
    ccb_student_ids = ccb_participants.select{ |x| x.status.downcase == 'member' }.map{ |x| x.id }

    # Load the students that TB already knows about. Cross referenced from the CCB group participant results.
    students = Student.includes(:person).where(people: { external_id: ccb_student_ids })

    # Load the facilitators that TB already knows about. Cross referenced from the CCB group participant results.
    facilitators = self.facilitators

    # Get arrays of just the IDs
    student_ids = students.map { |x| x.person.external_id.to_s }
    facilitator_ids = facilitators.map { |x| x.person.external_id.to_s }

    # Assign the students we have to the kata. This *should* be a safe update.
    self.students = students

    # If we already have the student or facilitator in TB, remove them from the CCB sync participant list,
    # we don't need to go get them
    ccb_participants.delete_if { |x| student_ids.include?(x.id) }
    ccb_participants.delete_if { |x| facilitator_ids.include?(x.id) }

    # Iterate through the remaining CCB group participants.
    # Create a user for them and add them to the Kata.
    ccb_participants.each do |participant|
      if participant.status.downcase == 'member'
        ccb_individual = Ccb::Individual.find(participant.id).first # TODO: find should return 1, not an array... of 1.
        self.students << Student.create_from_ccb(ccb_individual)
      else
        ccb_individual = Ccb::Individual.find(participant.id).first # TODO: find should return 1, not an array... of 1.
        facilitator = Facilitator.where(id: participant.id).first
        if facilitator.nil?
          facilitator = Facilitator.create_from_ccb(ccb_individual)
        end
        kata_facilitator = KataFacilitator.new({:title => participant.status, :facilitator => facilitator})
        self.kata_facilitators << kata_facilitator
      end
    end

  end

  def create_transcript
    sql = <<-SQL
      SELECT cev1.value firstName, cev2.value lastName, email.value email, address_1,
             address_2, city, state, postal_code, final_assessment
        FROM kata_learners
       INNER JOIN roles r on kata_learners.student_id = r.id
       INNER JOIN people p on r.person_id = p.id
       INNER JOIN contacts c on c.contactable_id = p.id
        LEFT JOIN contact_addresses ca on c.id = ca.contact_id and ca.name = 'mailing'
        LEFT JOIN contact_entity_values cev1 on cev1.contact_id = c.id and cev1.name = 'firstname'
        LEFT JOIN contact_entity_values cev2 on cev2.contact_id = c.id and cev2.name = 'lastName'
        LEFT JOIN contact_entity_values email on email.contact_id = c.id and email.name = 'Primary' and email.type = 'ContactEmail'
       WHERE kata_learners.kata_id = #{self.id}
    SQL

    csv_string = CSV.generate do |csv|
      results = Session.connection.execute sql
      csv << %w(firstname lastname address_1 address2 city state zipcode)
      results.each do |r|
        csv << r#[r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7]]
      end
    end

    return csv_string
  end

  def day_and_time
    @session_day_and_time ||= self.schedules.first
    return @session_day_and_time ? @session_day_and_time.date.localtime.strftime('%A, %l:%M%P') : nil
  end
end
