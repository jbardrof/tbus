# == Schema Information
#
# Table name: kata_learners
#
#  id               :integer          not null, primary key
#  kata_id          :integer
#  student_id       :integer
#  final_assessment :string(255)
#  enrolled_on      :date
#  widthdrawl_on    :date
#  created_at       :datetime
#  updated_at       :datetime
#

class KataLearner < ActiveRecord::Base
  belongs_to :session, foreign_key: 'kata_id'
  belongs_to :student

  has_many :kata_learner_assessments
  has_many :kata_learner_participations
end
