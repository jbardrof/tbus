# == Schema Information
#
# Table name: organizations
#
#  id             :integer          not null, primary key
#  parent_id      :integer
#  external_id    :string(255)
#  federal_tax_id :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#

class Organization < ActiveRecord::Base
  has_one :contact, as: :contactable, foreign_key: 'contactable_id'
  has_many :programs
  has_many :organizations, class_name: 'Organization', foreign_key: 'parent_id'
end
