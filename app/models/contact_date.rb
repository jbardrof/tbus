# == Schema Information
#
# Table name: contact_entity_values
#
#  id         :integer          not null, primary key
#  contact_id :integer
#  type       :string(255)
#  name       :string(255)
#  value      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class ContactDate < ContactEntityValue
end
