class Certificate < ActiveRecord::Base
  belongs_to :student, foreign_key: 'role_id'
  belongs_to :track
end
