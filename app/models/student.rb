# == Schema Information
#
# Table name: roles
#
#  id              :integer          not null, primary key
#  person_id       :integer
#  organization_id :integer
#  type            :string(255)
#  related_from    :datetime
#  related_thru    :datetime
#  created_at      :datetime
#  updated_at      :datetime
#

class Student < Role
  has_many :kata_learners
  has_many :sessions, through: :kata_learners

  has_one :proper_name, primary_key: 'person_id', foreign_key: 'contactable_id'

  scope :student_courses, -> {
    select('kata_learners.*')
    .includes(:kata_learners)
    .joins(:proper_name)
    .order('lastname desc, firstname desc')
  }

  scope :work_with_tracks, -> (id) {
    where(id: id)
    .includes({ kata_learners: [{ session: [ {course: [:track] } ] } ] })
    .joins({ kata_learners: [{ session: [ {course: [:track] } ] } ] })
    .order('tracks.id').first
  }

  def self.create_from_ccb(ccb_individual)
    person = Person.find_or_create_by external_id: ccb_individual.id

    person.contact ||= Contact.new
    person.contact.names = [ContactName.create(name: 'FirstName', value: ccb_individual.first_name),
                            ContactName.create(name: 'LastName', value: ccb_individual.last_name)]
    person.contact.phones = ccb_individual.phones.map { |e|
      ContactPhone.create(name: e.type, value: e.value) }

    person.contact.emails = [ContactEmail.create(name: 'Primary', value: ccb_individual.email)]

    ccb_individual.addresses.each do |addr|
      ca = ContactAddress.new(
        name:         addr.type,
        address_1:    addr.street_address,
        city:         addr.city,
        state:        addr.state,
        postal_code:  addr.zip)
      person.contact.addresses << ca if ca.save
    end

    new_student = create(related_from: Time.now)
    person.roles << new_student

    new_student
  end

  def courses_taken
    
  end

  def tracks
    t = self.sessions
      .joins(course: [:track])
      .includes(course: [:track])
      .group('tracks.id')
    #student_tracks = courses_taken.uniq { |c| c.track }.map { |c| c.track }

    #student_tracks.each do |st|
    #  st.courses = courses_taken.select { |ct| ct.track == st }
    #end
  end
end
