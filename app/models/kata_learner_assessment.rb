# == Schema Information
#
# Table name: kata_learner_assessments
#
#  id                 :integer          not null, primary key
#  kata_learner_id    :integer
#  kata_assessment_id :integer
#  grade              :string(255)
#  on_time            :boolean          default(TRUE)
#  created_at         :datetime
#  updated_at         :datetime
#

class KataLearnerAssessment < ActiveRecord::Base
  belongs_to :kata_learner
  belongs_to :kata_assessment
end
