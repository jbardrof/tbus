# == Schema Information
#
# Table name: grading_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class GradingType < ActiveRecord::Base
end
