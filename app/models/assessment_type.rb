# == Schema Information
#
# Table name: assessment_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class AssessmentType < ActiveRecord::Base
  has_many :course_assessments
  has_many :courses, through: :course_assessments

  validates :name, presence: true, length: { maximum: 255 }, format: { with: /\A[A-Za-z0-9\s]+\z/ }
  validates :description, length: { maximum: 255 }
end
