# == Schema Information
#
# Table name: kata_schedules
#
#  id          :integer          not null, primary key
#  kata_id     :integer
#  date        :datetime
#  duration    :integer
#  rescheduled :datetime
#  canceled    :boolean
#  created_at  :datetime
#  updated_at  :datetime
#  title       :string(255)
#

class KataSchedule < ActiveRecord::Base
  belongs_to :kata

  attr_accessor :repeats

  has_many :kata_learner_participations
  has_many :kata_schedule_resources
  has_many :resources, through: :kata_schedule_resources
  has_many :kata_assessments

  default_scope -> { order('date ASC') }

  def self.new_recurring(params)
    schedule_occurances = []
    params[:repeats].to_i.times do |i|
      occurance = params.clone
      occurance[:date] = params[:date].to_datetime + i.weeks

      schedule_occurances << occurance
    end

    return self.create(schedule_occurances)
  end

  def select_display
    "#{self.title} - #{I18n.l(date, format: :short)}"
  end
end
