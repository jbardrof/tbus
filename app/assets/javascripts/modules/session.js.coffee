
class @Session
  constructor: () ->

  show: () ->
    calendarDate = new Date()
    d = calendarDate.getDate()
    m = calendarDate.getMonth()
    y = calendarDate.getFullYear()
    cal = $('#calendar').fullCalendar(
      droppable: true
      editable: true
      selectable: true
      select: (start, end, allDay) =>
        $('.new_kata_schedule').unbind('submit')
        $('#scheduler .destroy').hide()
        $('#scheduler').modal()
        $('#scheduler #date').val start.format()
        $('.new_kata_schedule').submit (e) ->

          $this = $(e.currentTarget)
          e.preventDefault()
          $.ajax "#{session_base_url}/create_schedule",
            type: 'POST'
            data: $this.serialize()
            success: (data) ->
              if data.success
                for event in data.schedules
                  cal.fullCalendar 'renderEvent',
                    id: event.id
                    title: event.title
                    start: event.date
                    allDay: false,
                    true
                cal.fullCalendar 'unselect'
                $('#scheduler').modal('hide')
            complete: ->
              $('.new_kata_schedule').unbind('submit')

      eventClick: (calEvent, jsEvent, view) ->
        $('.new_kata_schedule').unbind('submit')
        $('#scheduler').modal()
        $('#scheduler .destroy').show()
        $('#scheduler #date').val calEvent.start.format()
        $('#scheduler #time').val calEvent.start.format('hh:mm')
        $('#scheduler h4').text calEvent.title
        $('#scheduler #kata_schedule_title').val calEvent.title

        $('.new_kata_schedule').submit (e) ->
          $this = $(e.currentTarget)
          e.preventDefault()

          $.ajax "#{session_base_url}/update_schedule/#{calEvent.id}",
            type: 'POST'
            data: $this.serialize()
            success: (data) ->
              if data.success
                calEvent.title = data.title
                calEvent.start = data.start
                cal.fullCalendar 'updateEvent', calEvent
                $('#scheduler').modal('hide')
            complete: ->
              $('.new_kata_schedule').unbind('submit')

        $('#scheduler .destroy').click (e) ->
          $this = $(e.currentTarget)
          e.preventDefault()

          $.ajax "#{session_base_url}/destroy_schedule/#{calEvent.id}",
            type: 'DELETE'
            success: (data) ->
              if data.success
                cal.fullCalendar 'removeEvents', calEvent.id
                $('#scheduler').modal('hide')
            complete: ->
              $('.new_kata_schedule').unbind('submit')


      drop: (date, allDay) ->
        copiedEventObject = undefined
        eventClass = undefined
        originalEventObject = undefined
        originalEventObject = $(this).data("eventObject")
        originalEventObject.id = Math.floor(Math.random() * 99999)
        eventClass = $(this).attr("data-event-class")
        console.log originalEventObject
        copiedEventObject = $.extend({}, originalEventObject)
        copiedEventObject.start = date
        copiedEventObject.allDay = allDay
        copiedEventObject["className"] = [eventClass]  if eventClass
        $(".full-calendar-demo").fullCalendar "renderEvent", copiedEventObject, true
        $(this).remove()  if $("#calendar-remove-after-drop").is(":checked")

      events: schedule
    )

    @tardyForm()

    @gradingForm()

    @attendanceForm()

    @finalAssessmentForm()

    $('.select2').on 'change', (e) =>
      $(e.target).parents('tr').find('input[type="submit"]').show()

    $('.assign-assessment').click (e) =>
      $this = $(e.target)
      e.preventDefault()


      $.ajax $this.parents('tr').find('form').attr('action'),
        type: 'POST'
        data:
          potential : $this.parents('tr').find('#potential').val(),
          schedule: $this.parents('tr').find('select').val(),
          assessment: $this.parents('tr').find('#course_assessment_id').val(),
          authenticity_token: $auth_token
        success: (data) ->
          console.log data

  gradingForm: () ->
    $('.learner-grading-form input[type="checkbox"]').change (e) =>
      $(e.target).parents('.learner-grading-form').submit()

    $('.learner-grading-form').submit (e) =>
      $this = $(e.target)
      e.preventDefault()

      $.ajax $this.attr('action'),
        type: 'POST'
        data: $this.serialize()
        beforeSend: (xhr, settings) ->
          $this.hide()
          $this.after('<i class="fa fa-spinner fa-spin attendance-progress"></i>')
        success: (data) ->
          console.log data
        complete: (xhr, status) ->
          $this.show()
          $this.parent().find('i.fa-spinner').remove()

  finalAssessmentForm: () ->
    $('.learner-final-assessment input').blur (e) =>
      $(e.currentTarget).parents('.learner-final-assessment').submit()

    $('.learner-final-assessment').submit (e) =>
      $this = $(e.target)
      e.preventDefault()

      $.ajax $this.attr('action'),
        type: 'POST'
        data: $this.serialize()
        beforeSend: (xhr, settings) ->
          $this.hide()
          $this.after('<i class="fa fa-spinner fa-spin attendance-progress"></i>')
        success: (data) ->
          console.log data
          $this.parent().find('.tardy').show()
        complete: (xhr, status) ->
          $this.show()
          $this.parent().find('i.fa-spinner').remove()

  attendanceForm: () ->
    $('input[type="checkbox"].attendanceBox').change (e) =>
      $target = $(e.target)
      schedule = $target.attr('data-schedule')
      learner = $target.attr('data-learner')
      selected = $target.is(':checked')

      $.ajax "#{session_base_url}/attendance",
        type: 'POST'
        data: { schedule : schedule, learner : learner, selected : selected, authenticity_token: $auth_token }
        beforeSend: (xhr, settings) ->
          $target.hide()
          $target.after('<i class="fa fa-spinner fa-spin attendance-progress"></i>')
        success: (data) ->
          console.log data
          $target.parent().find('.tardy').show()
        complete: (xhr, status) ->
          $target.show()
          $target.parent().find('i.fa-spinner').remove()

  tardyForm: () ->
    $('.tardy').each (i, e) =>
      $this = $(e)
      if $this.parent().find('input[type="checkbox"]').is(':checked')
        $this.show()

    $('.tardy .fa-clock-o').click (e) =>
      $target = $(e.target)
      schedule = $target.parent().parent().find('input[type="checkbox"]').attr('data-schedule')
      learner = $target.parent().parent().find('input[type="checkbox"]').attr('data-learner')
      tardy = if $target.hasClass('on-time') then 'late' else 'on-time'

      $.ajax "#{session_base_url}/tardy",
        type: 'POST'
        data: { status : tardy, schedule : schedule, learner : learner, authenticity_token : $auth_token }
        success: (data) ->
          if tardy == 'late'
            $target.removeClass('on-time').addClass('late')
          else if tardy == 'on-time'
            $target.removeClass('late').addClass('on-time')
