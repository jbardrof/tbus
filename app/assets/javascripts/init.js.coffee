# -------------------------------------------------------------------------------------------------
# Initialization of tools on DOM ready
ready = ->
  setSelect2()
  setDatepickers()

$(document).ready(ready)
$(document).on('page:load', ready)

# -------------------------------------------------------------------------------------------------
# select2
@setSelect2 = (selector = $(".select2")) ->
  selector.each (i, elem) ->
    $(elem).select2(
      dropdownAutoWidth: true
    )
# -------------------------------------------------------------------------------------------------

# Initialize date pickers
@setDatepickers = (selector = $('.with-datepicker')) ->
  selector.each (i, elem) ->
    $(elem).datepicker {
      dateFormat: 'mm/dd/yy'
      altField: '#isoDate'
      altFormat: 'YYYY-MM-DDTHH:mm:ssZ'
    }
