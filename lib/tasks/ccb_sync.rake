
namespace :sync do
  require "#{Rails.root}/config/initializers/colorizer"
  desc 'Synchronize TraveledBy with the organizations CCB information'
  task :ccb_to_tbus, [:group_name, :session, :allow_inactive] => [:environment] do |t, args|

    # Determine allow inactive flag
    allow_inactive = args[:allow_inactive] || false

    # Get the session we'll be syncing the groups with.
    session = load_kata_session_by_name args[:session]

    # Start with all the group profiles from CCB that match the given name
    group_profiles = load_groups_by_name args[:group_name]

    group_profiles.each do |profile|
      # If the group is inactive we don't want to import it to TraveledBy
      unless allow_inactive == false && profile.inactive?

        kata_name = profile.name.gsub(/\A[\w\s\-]*(#{Regexp.quote(args[:group_name])})\s/, "")
        kata = Session.find_or_create_by(name: kata_name, external_id: profile.id) do |k|
          k.kata_session_id = session.id
          k.kata_start = session.schedule_start
          k.kata_until = session.schedule_until

          puts "#{tb_light_green("Created:")} #{k.name}"
        end

        kata.synchronize_learners_from_ccb
      end
    end
  end

  def load_kata_session_by_name(name)
    session = Term.where(name: name).first

    unless session
      abort tb_red("FAIL: Unable to find the requested Kata Session")
    end

    return session
  end

  def load_groups_by_name(name)
    group_profiles = Ccb::GroupProfile.find_all_groups_by_name(name)

    unless group_profiles.any?
      abort tb_light_yellow("END: No Groups found to Sync")
    end

    return group_profiles
  end

  # For some ridiculous reason, the colorizer module doesn't load correctly....
  def tb_black(text);      colorize text, "0;30"; end
  def tb_red(text);        colorize text, "0;31"; end
  def tb_green(text);      colorize text, "0;32"; end
  def tb_brown(text);      colorize text, "0;33"; end
  def tb_blue(text);       colorize text, "0;34"; end
  def tb_purple(text);     colorize text, "0;35"; end
  def tb_cyan(text);       colorize text, "0;36"; end
  def tb_light_gray(text); colorize text, "0;37"; end

  def tb_dark_gray(text);    colorize text, "1;30"; end
  def tb_light_red(text);    colorize text, "1;31"; end
  def tb_light_green(text);  colorize text, "1;32"; end
  def tb_light_yellow(text); colorize text, "1;33"; end
  def tb_light_blue(text);   colorize text, "1;34"; end
  def tb_light_purple(text); colorize text, "1;35"; end
  def tb_light_cyan(text);   colorize text, "1;36"; end
  def tb_white(text);        colorize text, "1;37"; end

  # colors the text based on color code
  def colorize(text, color_code)
    "\033[#{color_code}m#{text}\033[0m"
  end
end
