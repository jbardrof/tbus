

namespace :tools do
  # - Traveled By script tooling
  #
  # The following tool section is for one off tools. Any scripts that need to be run
  # outside the regular application workflow can reside here. CRON job scheduled 
  # scripts should reside in the :jobs: namespace.
  #
  desc 'Assign courses to tracks using the prefix'
  task :assign_tracks => :environment do
    # - Track assignment
    #
    # Courses are not yet assigned to tracks, or not all have been assigned, 
    # this task will find all unassigned courses and find their track based on their prefix
    # This assumes all tracks entered to the system have the proper short_code assigned.
    # With the shortcode and the course number prefixes, we can properly assign tracks.
    #
    tracks = Track.all
    courses = Course.where track_id: nil

    courses.each do |c|
      # Use find instead of select. Find will return after the first result.
      c.track = tracks.find{ |x| c.number.include?(x.short_code) }
      c.save unless c.track == nil
    end
  
  end
end
